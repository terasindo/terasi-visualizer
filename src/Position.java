/**
 * Created by gyosh on 12/19/15.
 */
public class Position {
    public static final float MIN_LATITUDE = -6.098996f;
    public static final float MIN_LONGITUDE = 106.689431f;
    public static final float MAX_LATITUDE = -6.353604f;
    public static final float MAX_LONGITUDE = 106.975076f;
    public static final float LATITUDE_RANGE = MAX_LATITUDE - MIN_LATITUDE;
    public static final float LONGITUDE_RANGE = MAX_LONGITUDE - MIN_LONGITUDE;

    private float longitude;
    private float latitude;

    public Position(float longitude, float latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public float getX() {
        return (longitude - MIN_LONGITUDE) / LONGITUDE_RANGE;
    }

    public float getY() {
        return (latitude - MIN_LATITUDE) / LATITUDE_RANGE;
    }
}
