import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by gyosh on 12/19/15.
 */
public class Main {
    private JSlider seekBar;
    private JButton playOrPause;
    private JButton stop;
    private JButton speedIncrease;
    private JButton speedDecrease;
    private JButton load;
    private JPanel main;
    private JPanel canvas;
    private JPanel navBar;
    private JLabel timeDisplay;
    private JTextField busFilter;
    private JCheckBox showBusId;
    private JTextField busStopFilter;
    private JCheckBox showBusStopInitial;
    private JButton previousFrame;
    private JButton nextFrame;
    private JTextArea polylineInput;
    private JTextField showWithinAoe;

    private Timer timer;
    private int currentTime;
    private int timerDelay;
    private double playSpeed;

    public Main() {
        currentTime = 0;
        playSpeed = 1;
        timerDelay = 100;

        timer = new Timer(timerDelay, this::nextTick);
        playOrPause.addActionListener(this::playOrPauseAction);
        stop.addActionListener(this::stopAction);
        speedIncrease.addActionListener(this::speedIncreaseAction);
        speedDecrease.addActionListener(this::speedDecreaseAction);
        load.addActionListener(this::loadAction);
        previousFrame.addActionListener(this::previousFrameAction);
        nextFrame.addActionListener(this::nextFrameAction);

        seekBar.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                timer.stop();
                updatePlayOrPauseText();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                currentTime = seekBar.getValue();
                timer.start();
                updatePlayOrPauseText();
            }
        });

        // Filter
        showBusId.addActionListener(this::setFilterAction);
        showBusStopInitial.addActionListener(this::setFilterAction);
        busFilter.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
            }
            public void insertUpdate(DocumentEvent e) {
                setFilterAction(null);
            }
            public void removeUpdate(DocumentEvent e) {
                setFilterAction(null);
            }
        });
        busStopFilter.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
            }
            public void insertUpdate(DocumentEvent e) {
                setFilterAction(null);
            }
            public void removeUpdate(DocumentEvent e) {
                setFilterAction(null);
            }
        });
        showWithinAoe.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
            }
            public void insertUpdate(DocumentEvent e) {
                setFilterAction(null);
            }
            public void removeUpdate(DocumentEvent e) {
                setFilterAction(null);
            }
        });

        updateTimeAndSpeedLabel();
        polylineInput.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);

                try {
                    String[] textInputValue = polylineInput.getText().split("\n");
                    float[][] values = new float[textInputValue.length][];

                    for (int i = 0; i < textInputValue.length; i++) {
                        String[] lineValue = textInputValue[i].split("#");

                        float[] value = new float[lineValue.length];
                        for (int j = 0; j < lineValue.length; j++) {
                            value[j] = Float.parseFloat(lineValue[j]);
                        }

                        values[i] = value;
                    }
                    ((Canvas) canvas).setPolylineToDraw(values);
                } catch (Exception err) {
                    System.out.println(err.toString());
                }
            }
        });
    }

    private void loadFile(File file) {
        ArrayList<BusInfo> busInfos =  new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));

            String line = br.readLine();
            while (line != null) {
                BusInfo busInfo = new BusInfo(line);
                busInfos.add(busInfo);

                line = br.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ((Canvas)canvas).setBusInfos(busInfos);
        System.out.println("DONE");
    }

    private void updatePlayOrPauseText() {
        if (timer.isRunning()) {
            playOrPause.setText("Pause");
        } else {
            playOrPause.setText("Play");
        }
    }

    private void updateTimeAndSpeedLabel() {
        int hour = currentTime / 60;
        int minute = currentTime % 60;
        timeDisplay.setText(String.format("%d / %02d:%02d (%.2fx)", currentTime, hour, minute, playSpeed));
    }

    private void updateSeekBar() {
        seekBar.setValue(currentTime);
    }

    private void nextTick(ActionEvent event) {
        currentTime = Math.min(currentTime, BusInfo.totalTime);

        updateTimeAndSpeedLabel();
        updateSeekBar();

        Canvas canvasPanel = (Canvas) canvas;
        canvasPanel.setCurrentTime(currentTime);
        canvasPanel.actionPerformed(event);

        currentTime = Math.min(currentTime + 1, BusInfo.totalTime);
    }

    private void playOrPauseAction(ActionEvent e) {
        if (timer.isRunning()) {
            timer.stop();
        } else {
            timer.start();
        }
        updatePlayOrPauseText();
    }

    private void stopAction(ActionEvent e) {
        timer.stop();
        currentTime = 0;
        updatePlayOrPauseText();
        updateSeekBar();
        updateTimeAndSpeedLabel();
    }

    private void speedIncreaseAction(ActionEvent e) {
        playSpeed *= 2;
        timerDelay /= 2;
        timer.setDelay(timerDelay);
    }
    private void speedDecreaseAction(ActionEvent e) {
        playSpeed /= 2;
        timerDelay *= 2;
        timer.setDelay(timerDelay);
    }

    private void nextFrameAction(ActionEvent e) {
        timer.stop();
        updatePlayOrPauseText();
        updateSeekBar();
        updateTimeAndSpeedLabel();
        nextTick(e);
    }
    private void previousFrameAction(ActionEvent e) {
        timer.stop();
        updatePlayOrPauseText();
        updateSeekBar();
        updateTimeAndSpeedLabel();

        currentTime -= 2; // Mohohoho
        nextTick(e);
    }

    private String[] getFilterTokens(JTextField textField) {
        String[] tokens;
        if (textField.getText().length() == 0) {
            tokens = new String[0];
        } else {
            tokens = textField.getText().split(",");
        }
        return tokens;
    }

    private void setFilterAction(ActionEvent e) {
        Canvas canvasPanel = (Canvas) canvas;

        canvasPanel.setBusOrCorridorFilter(getFilterTokens(busFilter));
        canvasPanel.setBusStopFilter(getFilterTokens(busStopFilter));
        canvasPanel.setShowWithinAoe(getFilterTokens(showWithinAoe));
        canvasPanel.setShowBusId(showBusId.isSelected());
        canvasPanel.setShowBusStopInitial(showBusStopInitial.isSelected());

        canvasPanel.actionPerformed(e);
    }

    private void loadAction(ActionEvent e) {
        final JFileChooser fileChooser = new JFileChooser();

        int returnVal = fileChooser.showOpenDialog(main);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File inputFile = fileChooser.getSelectedFile();
            loadFile(inputFile);

            ((Canvas) canvas).actionPerformed(e);
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Terasi Visualizer");

        Main main = new Main();
        frame.setContentPane(main.main);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        canvas = new Canvas();
    }
}
