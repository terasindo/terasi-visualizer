/**
 * Created by gyosh on 12/19/15.
 */
public class BusInfo {
    public static final int totalTime = 24 * 60;

    private String id;
    private String corridor;
    private Position[] positions;

    public BusInfo(String encodedString) {
        encodedString = encodedString + "0";
        String[] tokens = encodedString.split("#");

        int p = 0;
        this.id = tokens[p++];
        this.corridor = tokens[p++];

        positions = new Position[totalTime];
        for (int i = 0; i < totalTime; i++) {
            String longitude = tokens[p++];
            String latitude = tokens[p++];

            try {
                float fLongitude = Float.parseFloat(longitude);
                float fLatitude = Float.parseFloat(latitude);

                positions[i] = new Position(fLongitude, fLatitude);
            } catch (NumberFormatException e) {
                positions[i] = null;
            }
        }
    }

    public String getId() {
        return id;
    }

    public String getCorridor() {
        return corridor;
    }

    public Position getPosition(int time) {
        if (time < 0 || time >= totalTime) {
            return null;
        }

        return positions[time];
    }
}
