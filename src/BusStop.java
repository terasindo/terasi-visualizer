import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by gyosh on 12/24/15.
 */
public class BusStop {
    private String name;
    private String initial;
    private Position position;
    private ArrayList<String> serves;

    public BusStop(String name, Position position, String serves) {
        this.name = name;
        this.position = position;
        this.initial = createInitial(name);

        this.serves = new ArrayList<>();
        String tokens[] = serves.split(",");
        for (String token : tokens) {
            this.serves.add(token);
        }
    }

    public String getName() {
        return name;
    }

    public String getInitial() {
        return initial;
    }

    public Position getPosition() {
        return position;
    }

    public ArrayList<String> getServes() {
        return serves;
    }

    private String createInitial(String str) {
        String tokens[] = str.split(" ");
        String initial = "";
        for (String token : tokens) {
            initial += token.charAt(0);
        }

        return initial;
    }
}
