import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by gyosh on 12/19/15.
 */
public class Canvas extends JPanel implements ActionListener {
    private static final int POINT_RADIUS = 4;

    private ArrayList<BusInfo> busInfos;
    private ArrayList<BusStop> busStops;
    private int currentTime;
    private Font font;
    private Image map;

    private HashMap<String, Color> corridorColorMap;
    private HashSet<String> showOnlyBusOrCorridor;
    private HashSet<String> showOnlyBusStop;
    private boolean showBusId;
    private boolean showBusStopInitial;

    private float[][] polylineToDraw;

    private Position aoePosition;
    private int aoeRadius;

    public Canvas() {
        try {
            // If this runs on jar, we get the file in this way
            map = ImageIO.read(this.getClass().getResource("/map.png"));
        } catch (Exception e) {
            // Not in jar, get the file as usual
            map = new ImageIcon("map.png").getImage();
        }

        font = new Font("Sans", Font.BOLD, 10);
        initializeCorridorColorMap();
        initializeBusStops();
    }

    public void setPolylineToDraw(float[][] val) {
        polylineToDraw = val;
    }

    public void setCurrentTime(int currentTime) {
        this.currentTime = currentTime;
    }

    public void setBusInfos(ArrayList<BusInfo> busInfos) {
        this.busInfos = busInfos;
    }

    public void setBusOrCorridorFilter(String[] filterTokens) {
        showOnlyBusOrCorridor = null;
        if (filterTokens.length > 0) {
            showOnlyBusOrCorridor = new HashSet<>();
            for (String filterToken : filterTokens) {
                showOnlyBusOrCorridor.add(filterToken);
            }
        }
    }

    public void setBusStopFilter(String[] filterTokens) {
        showOnlyBusStop = null;
        if (filterTokens.length > 0) {
            showOnlyBusStop = new HashSet<>();
            for (String filterToken : filterTokens) {
                showOnlyBusStop.add(filterToken);
            }
        }
    }

    public void setShowWithinAoe(String[] filterTokens) {
        aoePosition = null;
        if (filterTokens.length >= 3) {
            float lon = Float.parseFloat(filterTokens[0]);
            float lat = Float.parseFloat(filterTokens[1]);

            aoePosition = new Position(lon, lat);
            aoeRadius = (int)Float.parseFloat(filterTokens[2]);

        }
    }

    public void setShowBusId(boolean showBusId) {
        this.showBusId = showBusId;
    }

    public void setShowBusStopInitial(boolean showBusStopInitial) {
        this.showBusStopInitial = showBusStopInitial;
    }

    private boolean needToShow(BusInfo busInfo) {
        return (showOnlyBusOrCorridor == null) || showOnlyBusOrCorridor.contains(busInfo.getCorridor()) || showOnlyBusOrCorridor.contains(busInfo.getId());
    }

    private boolean needToShow(BusStop busStop) {
        ArrayList<String> serves = busStop.getServes();

        if (showOnlyBusStop == null) {
            return false;
        }

        for (String corridor : serves) {
            if (showOnlyBusStop.contains(corridor)) {
                return true;
            }
        }
        return false;
    }

    private String formatBusText(BusInfo busInfo) {
        if (showBusId) {
            return String.format(".%s-%s", busInfo.getCorridor(), busInfo.getId());
        } else {
            return String.format(".%s", busInfo.getCorridor());
        }
    }

    private String formatBusStopText(BusStop busStop) {
        if (showBusStopInitial) {
            return String.format(".%s", busStop.getInitial());
        } else {
            return ".";
        }
    }

    private void draw(Graphics g) {
        int width = this.getWidth();
        int height = this.getHeight();

        Graphics2D g2d = (Graphics2D) g;
        g2d.drawImage(map, 0, 0, map.getWidth(null), map.getHeight(null), null);
        g2d.setFont(font);

        if (polylineToDraw != null) {
            for (int id = 0; id < polylineToDraw.length; id++) {
                float[] polyline = polylineToDraw[id];
                int n = polyline.length / 2;
                for (int i = 1; i < n; i++) {
                    int p = i-1;
                    Position prev = new Position(polyline[2*p], polyline[2*p+1]);
                    Position next = new Position(polyline[2*i], polyline[2*i+1]);

                    g2d.setColor(Color.BLACK);
                    g2d.fillOval((int)(prev.getX()*width) - POINT_RADIUS/2, (int)(prev.getY()*height) - POINT_RADIUS/2, POINT_RADIUS, POINT_RADIUS);
                    g2d.fillOval((int)(next.getX()*width) - POINT_RADIUS/2, (int)(next.getY()*height) - POINT_RADIUS/2, POINT_RADIUS, POINT_RADIUS);

                    g2d.drawLine((int)(prev.getX()*width), (int)(prev.getY()*height), (int)(next.getX()* width), (int)(next.getY()* height));
                }
            }
        }

        int aoeCentroidX = 0;
        int aoeCentroidY = 0;

        if (aoePosition != null) {
            aoeCentroidX = (int)(aoePosition.getX()*width);
            aoeCentroidY = (int)(aoePosition.getY()*height);

            g2d.setColor(Color.BLUE);
            g2d.fillOval(aoeCentroidX-POINT_RADIUS/2, aoeCentroidY-POINT_RADIUS/2, POINT_RADIUS, POINT_RADIUS);
            g2d.drawOval(aoeCentroidX-aoeRadius, aoeCentroidY-aoeRadius, 2*aoeRadius, 2*aoeRadius);
        }

        if (busInfos == null) return;

        for (BusStop busStop : busStops) {
            if (needToShow(busStop)) {
                Position pos = busStop.getPosition();

                int x = Math.round(pos.getX() * width);
                int y = Math.round(pos.getY() * height);

                g2d.setColor(Color.BLACK);
                g2d.drawString(formatBusStopText(busStop), x, y);
            }
        }

        for (BusInfo busInfo : busInfos) {
            if (needToShow(busInfo)) {
                Position pos = busInfo.getPosition(currentTime);
                if (pos != null) {
                    int x = Math.round(pos.getX() * width);
                    int y = Math.round(pos.getY() * height);

                    // Sorry, got to refactor later to be put in needToShow(busInfo)
                    boolean show = true;
                    if (aoePosition != null) {
                        int dx = aoeCentroidX - x;
                        int dy = aoeCentroidY - y;

                        if (dx*dx + dy*dy > aoeRadius*aoeRadius) {
                            show = false;
                        }
                    }

                    if (show) {
                        g2d.setColor(corridorColorMap.get(busInfo.getCorridor()));
                        g2d.drawString(formatBusText(busInfo), x, y);
                    }
                }
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        draw(g);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
    }

    private void initializeCorridorColorMap() {
        corridorColorMap = new HashMap<>();

        corridorColorMap.put("1", new Color(0xd90102));
        corridorColorMap.put("2", new Color(0x0624ee));
        corridorColorMap.put("3", new Color(0x333333));
        corridorColorMap.put("4", new Color(0x1c0392));
        corridorColorMap.put("5", new Color(0xce3400));
        corridorColorMap.put("6", new Color(0x9a005a));
        corridorColorMap.put("7", new Color(0xff068a));
        corridorColorMap.put("8", new Color(0xdf12fa));
        corridorColorMap.put("9", new Color(0x500010));
        corridorColorMap.put("10", new Color(0x7a0204));
        corridorColorMap.put("11", new Color(0x1639f7));
        corridorColorMap.put("12", new Color(0x000000));
        corridorColorMap.put("2B", new Color(0x609df0));
        corridorColorMap.put("3A", new Color(0x005423));
        corridorColorMap.put("5A", new Color(0x3316fa));
        corridorColorMap.put("6A", new Color(0x005963));
        corridorColorMap.put("6B", new Color(0xf9380d));
        corridorColorMap.put("7A", new Color(0x003063));
        corridorColorMap.put("7B", new Color(0xfc7506));
        corridorColorMap.put("8A", new Color(0x6b05a7));
        corridorColorMap.put("9A", new Color(0x6ca80c));
    }
    private void initializeBusStops() {
        busStops = new ArrayList<>();

        busStops.add(new BusStop("Ahmad Yani Bea Cukai", new Position(106.873766f, -6.205516f), "10"));
        busStops.add(new BusStop("Ancol", new Position(106.83042f, -6.12753f), "5,5A,7B"));
        busStops.add(new BusStop("ASMI", new Position(106.888729f, -6.171752f), "2,2A,2B"));
        busStops.add(new BusStop("Atrium", new Position(106.84081f, -6.177103f), "2,2A,2B"));
        busStops.add(new BusStop("Balai Kota", new Position(106.82765f, -6.18027f), "2,2A,2B"));
        busStops.add(new BusStop("Bandengan Selatan", new Position(106.802919f, -6.136313f), "12"));
        busStops.add(new BusStop("Bank Indonesia", new Position(106.822959f, -6.182701f), "1,2A,3A,6A"));
        busStops.add(new BusStop("Bendungan Hilir", new Position(106.815307f, -6.217046f), "1,2A,3A"));
        busStops.add(new BusStop("Bermis", new Position(106.898499f, -6.178141f), "2,2A,2B"));
        busStops.add(new BusStop("Bidara Cina", new Position(106.867195f, -6.22972f), "7,7A,7B"));
        busStops.add(new BusStop("BKN", new Position(106.869908f, -6.257833f), "10,7,7A,7B,9A"));
        busStops.add(new BusStop("Blok M", new Position(106.801933f, -6.24326f), "1"));
        busStops.add(new BusStop("BNN", new Position(106.872086f, -6.246151f), "7,7A,7B,9,9A"));
        busStops.add(new BusStop("Buaran", new Position(106.917684f, -6.215126f), "11"));
        busStops.add(new BusStop("Budi Utomo", new Position(106.839059f, -6.165979f), "5,7B"));
        busStops.add(new BusStop("Buncit Indah", new Position(106.830276f, -6.274322f), "6,6A,6B"));
        busStops.add(new BusStop("Bundaran HI", new Position(106.823017f, -6.193607f), "1,2A,3A,6A"));
        busStops.add(new BusStop("Bundaran Senayan", new Position(106.801407f, -6.227414f), "1,2A,3A"));
        busStops.add(new BusStop("Cawang Ciliwung", new Position(106.863055f, -6.243082f), "9,9A"));
        busStops.add(new BusStop("Cawang Otista", new Position(106.86872f, -6.243608f), "7,7A,7B"));
        busStops.add(new BusStop("Cawang Sutoyo", new Position(106.875924f, -6.244835f), "10"));
        busStops.add(new BusStop("Cawang UKI", new Position(106.873576f, -6.250342f), "10,7,7A,9,9A"));
        busStops.add(new BusStop("Cempaka Mas 2", new Position(106.879152f, -6.165888f), "10"));
        busStops.add(new BusStop("Cempaka Putih", new Position(106.876376f, -6.174339f), "10"));
        busStops.add(new BusStop("Cempaka Tengah", new Position(106.866615f, -6.170365f), "2,2A,2B"));
        busStops.add(new BusStop("Cempaka Timur", new Position(106.876068f, -6.166528f), "2,2A,2B"));
        busStops.add(new BusStop("Cikoko Stasiun Cawang", new Position(106.857663f, -6.243326f), "9,9A"));
        busStops.add(new BusStop("Cipinang", new Position(106.889853f, -6.213644f), "11"));
        busStops.add(new BusStop("Cipinang Kebon Nanas", new Position(106.874714f, -6.223995f), "10"));
        busStops.add(new BusStop("Danau Agung", new Position(106.858069f, -6.146841f), "12"));
        busStops.add(new BusStop("Departemen Kesehatan", new Position(106.833193f, -6.228609f), "6,6A,6B"));
        busStops.add(new BusStop("Departemen Pertanian", new Position(106.822139f, -6.294496f), "6,6A,6B"));
        busStops.add(new BusStop("DEPLU", new Position(106.83475f, -6.173575f), "2,2A,2B"));
        busStops.add(new BusStop("Dispenda", new Position(106.738038f, -6.154588f), "2B,3,3A"));
        busStops.add(new BusStop("Dukuh Atas 1", new Position(106.822254f, -6.205569f), "1,2A,3A"));
        busStops.add(new BusStop("Dukuh Atas 2", new Position(106.82348f, -6.204292f), "4,6"));
        busStops.add(new BusStop("Duren Tiga", new Position(106.826931f, -6.252328f), "6,6A,6B"));
        busStops.add(new BusStop("Duri Kepa", new Position(106.768447f, -6.185354f), "8"));
        busStops.add(new BusStop("Enggano", new Position(106.892465f, -6.110126f), "10,12"));
        busStops.add(new BusStop("Flyover Jatinegara", new Position(106.873449f, -6.215169f), "10,11"));
        busStops.add(new BusStop("Flyover Klender", new Position(106.902913f, -6.213481f), "11"));
        busStops.add(new BusStop("Flyover Radin Inten", new Position(106.924475f, -6.21597f), "11"));
        busStops.add(new BusStop("Flyover Raya Bogor", new Position(106.865606f, -6.306544f), "7"));
        busStops.add(new BusStop("Galur", new Position(106.854771f, -6.17438f), "2,2A,2B"));
        busStops.add(new BusStop("Gambir 1", new Position(106.830378f, -6.174534f), "2,2A,2B"));
        busStops.add(new BusStop("Gambir 2", new Position(106.831f, -6.17889f), "2,2A,2B"));
        busStops.add(new BusStop("Gatot Subroto Jamsostek", new Position(106.821755f, -6.233046f), "9,9A"));
        busStops.add(new BusStop("Gatot Subroto LIPI", new Position(106.817542f, -6.227003f), "9,9A"));
        busStops.add(new BusStop("Gedong Panjang", new Position(106.805945f, -6.132617f), "12"));
        busStops.add(new BusStop("Gelanggang Remaja", new Position(106.867931f, -6.235462f), "7,7A,7B"));
        busStops.add(new BusStop("Gelora Bung Karno", new Position(106.805792f, -6.224228f), "1,2A,3A"));
        busStops.add(new BusStop("Glodok", new Position(106.815494f, -6.144808f), "1"));
        busStops.add(new BusStop("GOR Sumantri", new Position(106.832042f, -6.220829f), "6,6A,6B"));
        busStops.add(new BusStop("Grogol 1", new Position(106.78971f, -6.166807f), "2B,3,3A,8"));
        busStops.add(new BusStop("Grogol 2", new Position(106.788577f, -6.167398f), "8A,9,9A"));
        busStops.add(new BusStop("Gunung Sahari Mangga Dua", new Position(106.832454f, -6.137206f), "12,5,5A,7B"));
        busStops.add(new BusStop("Halimun", new Position(106.8334f, -6.205113f), "4,6,6B"));
        busStops.add(new BusStop("Harmoni", new Position(106.820425f, -6.165817f), "1,2,2B,3,3A,5A,7A,8,8A"));
        busStops.add(new BusStop("Imigrasi Jakarta Selatan", new Position(106.82799f, -6.256715f), "6,6A,6B"));
        busStops.add(new BusStop("Imigrasi Jakarta Timur", new Position(106.882f, -6.21463f), "11"));
        busStops.add(new BusStop("Indosiar", new Position(106.775381f, -6.163458f), "2B,3,3A"));
        busStops.add(new BusStop("Istiqlal", new Position(106.831017f, -6.172273f), "2,2A,2B"));
        busStops.add(new BusStop("ITC Mangga Dua", new Position(106.825958f, -6.136152f), "12"));
        busStops.add(new BusStop("Jatinegara RS Premier", new Position(106.868045f, -6.221951f), "11,5,7A,7B"));
        busStops.add(new BusStop("Jatipadang", new Position(106.826155f, -6.285627f), "6,6A,6B"));
        busStops.add(new BusStop("Jelambar", new Position(106.786743f, -6.166561f), "2B,3,3A"));
        busStops.add(new BusStop("Jembatan Baru", new Position(106.730508f, -6.15484f), "2B,3,3A"));
        busStops.add(new BusStop("Jembatan Besi", new Position(106.794915f, -6.151997f), "9"));
        busStops.add(new BusStop("Jembatan Dua", new Position(106.793518f, -6.143248f), "9"));
        busStops.add(new BusStop("Jembatan Gantung", new Position(106.749171f, -6.155477f), "2B,3,3A"));
        busStops.add(new BusStop("Jembatan Merah", new Position(106.834279f, -6.146775f), "12,5,5A,7B"));
        busStops.add(new BusStop("Jembatan Tiga", new Position(106.792729f, -6.133329f), "9"));
        busStops.add(new BusStop("Juanda", new Position(106.830707f, -6.168046f), "2,2A,2B,3,5A,7A"));
        busStops.add(new BusStop("Kali Besar Barat", new Position(106.811461f, -6.135214f), "12"));
        busStops.add(new BusStop("Kalideres", new Position(106.705909f, -6.154605f), "2B,3,3A"));
        busStops.add(new BusStop("Kampung Melayu", new Position(106.866909f, -6.224401f), "11,5,7,7A,7B"));
        busStops.add(new BusStop("Kampung Rambutan", new Position(106.881456f, -6.308744f), "7"));
        busStops.add(new BusStop("Kampung Sumur", new Position(106.911888f, -6.214406f), "11"));
        busStops.add(new BusStop("Karet", new Position(106.820005f, -6.21248f), "1,2A,3A"));
        busStops.add(new BusStop("Karet Kuningan", new Position(106.830902f, -6.217281f), "6,6A,6B"));
        busStops.add(new BusStop("Kayu Putih Rawasari", new Position(106.875399f, -6.187301f), "10"));
        busStops.add(new BusStop("Kebayoran Lama Bungur", new Position(106.781656f, -6.252819f), "8"));
        busStops.add(new BusStop("Kebon Jeruk", new Position(106.768885f, -6.194269f), "8"));
        busStops.add(new BusStop("Kebon Pala", new Position(106.861274f, -6.213067f), "5,7A,7B"));
        busStops.add(new BusStop("Kedoya Assiddiqiyah", new Position(106.765838f, -6.174534f), "8"));
        busStops.add(new BusStop("Kedoya Green Garden", new Position(106.763014f, -6.164499f), "8"));
        busStops.add(new BusStop("Kelapa Dua Sasak", new Position(106.769525f, -6.205554f), "8"));
        busStops.add(new BusStop("Kemayoran Landas Pacu Timur", new Position(106.854267f, -6.15215f), "12"));
        busStops.add(new BusStop("Kota", new Position(106.813952f, -6.137757f), "1,12"));
        busStops.add(new BusStop("Kramat Sentiong NU", new Position(106.846061f, -6.188174f), "5,7A,7B"));
        busStops.add(new BusStop("Kuningan Barat", new Position(106.828075f, -6.23732f), "9,9A"));
        busStops.add(new BusStop("Kuningan Madya", new Position(106.830438f, -6.21263f), "6,6A,6B"));
        busStops.add(new BusStop("Kuningan Timur", new Position(106.827882f, -6.235899f), "6,6A,6B"));
        busStops.add(new BusStop("Kwitang", new Position(106.838585f, -6.181127f), "2,2A,2B"));
        busStops.add(new BusStop("Latuharhari", new Position(106.827672f, -6.203122f), "6,6B"));
        busStops.add(new BusStop("Layur", new Position(106.899f, -6.19353f), "4,6B"));
        busStops.add(new BusStop("Lebak Bulus", new Position(106.774624f, -6.289548f), "8"));
        busStops.add(new BusStop("Mampang Prapatan", new Position(106.825696f, -6.241882f), "6,6A,6B"));
        busStops.add(new BusStop("Mangga Besar", new Position(106.817339f, -6.1521f), "1"));
        busStops.add(new BusStop("Manggarai", new Position(106.847504f, -6.208845f), "4,6B"));
        busStops.add(new BusStop("Masjid Agung", new Position(106.798371f, -6.235033f), "1"));
        busStops.add(new BusStop("Matraman 1", new Position(106.854465f, -6.20005f), "5,7A,7B"));
        busStops.add(new BusStop("Matraman 2", new Position(106.854273f, -6.199068f), "4,6B"));
        busStops.add(new BusStop("Monumen Nasional", new Position(106.822843f, -6.176258f), "1,2,2A,2B,3A,6A"));
        busStops.add(new BusStop("Museum Fatahillah", new Position(106.812032f, -6.133889f), "12"));
        busStops.add(new BusStop("Olimo", new Position(106.816714f, -6.149206f), "1"));
        busStops.add(new BusStop("Pademangan", new Position(106.831621f, -6.133636f), "5,5A,7B"));
        busStops.add(new BusStop("Pakin", new Position(106.804744f, -6.127961f), "12"));
        busStops.add(new BusStop("Pal Putih", new Position(106.84389f, -6.184417f), "5,7A,7B"));
        busStops.add(new BusStop("Pancoran Barat", new Position(106.837705f, -6.241609f), "9,9A"));
        busStops.add(new BusStop("Pancoran Tugu", new Position(106.844002f, -6.243081f), "9,9A"));
        busStops.add(new BusStop("Pangeran Jayakarta", new Position(106.817845f, -6.1376f), "12"));
        busStops.add(new BusStop("Pasar Baru", new Position(106.834873f, -6.166177f), "3,5A,7A"));
        busStops.add(new BusStop("Pasar Baru Timur", new Position(106.838166f, -6.162381f), "5,5A,7B"));
        busStops.add(new BusStop("Pasar Cempaka Putih", new Position(106.862512f, -6.172275f), "2,2A,2B"));
        busStops.add(new BusStop("Pasar Enjo", new Position(106.878215f, -6.214847f), "11"));
        busStops.add(new BusStop("Pasar Genjing", new Position(106.860894f, -6.194443f), "4,6B"));
        busStops.add(new BusStop("Pasar Induk", new Position(106.872017f, -6.294301f), "7"));
        busStops.add(new BusStop("Pasar Jatinegara", new Position(106.866266f, -6.215648f), "5,7A,7B"));
        busStops.add(new BusStop("Pasar Kebayoran Lama", new Position(106.783209f, -6.238444f), "8"));
        busStops.add(new BusStop("Pasar Kramat Jati", new Position(106.866534f, -6.268736f), "7"));
        busStops.add(new BusStop("Pasar Pulogadung", new Position(106.9059f, -6.187426f), "4,6B"));
        busStops.add(new BusStop("Pasar Rumput", new Position(106.841011f, -6.207068f), "4,6B"));
        busStops.add(new BusStop("Patra Kuningan", new Position(106.831269f, -6.2335f), "6,6A,6B"));
        busStops.add(new BusStop("Pecenongan", new Position(106.828149f, -6.167653f), "2,2A,2B,3,5A,7A"));
        busStops.add(new BusStop("Pedati Prumpung", new Position(106.874373f, -6.220197f), "10"));
        busStops.add(new BusStop("Pedongkelan", new Position(106.882499f, -6.167962f), "2,2A,2B"));
        busStops.add(new BusStop("Pejaten", new Position(106.829773f, -6.278307f), "6,6A,6B"));
        busStops.add(new BusStop("Pemuda Pramuka", new Position(106.874925f, -6.19283f), "10"));
        busStops.add(new BusStop("Pemuda Rawamangun", new Position(106.89167f, -6.193467f), "4,6B"));
        busStops.add(new BusStop("Penas Kalimalang", new Position(106.878037f, -6.239155f), "10"));
        busStops.add(new BusStop("Penggilingan", new Position(106.939587f, -6.213941f), "11"));
        busStops.add(new BusStop("Penjaringan", new Position(106.792f, -6.12629f), "12,9"));
        busStops.add(new BusStop("Permai Koja", new Position(106.893073f, -6.113877f), "10,12"));
        busStops.add(new BusStop("Permata Hijau", new Position(106.783301f, -6.221467f), "8"));
        busStops.add(new BusStop("Perumnas Klender", new Position(106.931f, -6.2167f), "11"));
        busStops.add(new BusStop("Pesakih", new Position(106.715236f, -6.154759f), "2B,3,3A"));
        busStops.add(new BusStop("Petojo", new Position(106.816971f, -6.169998f), "8A"));
        busStops.add(new BusStop("PGC 1", new Position(106.866325f, -6.262539f), "7,7A,7B"));
        busStops.add(new BusStop("PGC 2", new Position(106.865994f, -6.261894f), "10,9A"));
        busStops.add(new BusStop("Pinang Ranti", new Position(106.886416f, -6.290967f), "9"));
        busStops.add(new BusStop("Pluit", new Position(106.79108f, -6.115785f), "12,9"));
        busStops.add(new BusStop("Pluit Auto Plaza Landmark", new Position(106.799647f, -6.125234f), "12"));
        busStops.add(new BusStop("Plumpang Pertamina", new Position(106.893719f, -6.128647f), "10,12"));
        busStops.add(new BusStop("Polda Metro Jaya", new Position(106.809809f, -6.221451f), "1,2A,3A"));
        busStops.add(new BusStop("Pondok Indah 1", new Position(106.77925f, -6.287398f), "8"));
        busStops.add(new BusStop("Pondok Indah 2", new Position(106.783611f, -6.267216f), "8"));
        busStops.add(new BusStop("Pondok Pinang", new Position(106.772f, -6.28217f), "8"));
        busStops.add(new BusStop("Pos Pengumben", new Position(106.772271f, -6.21294f), "8"));
        busStops.add(new BusStop("Pramuka BPKP", new Position(106.874f, -6.19218f), "4,6B"));
        busStops.add(new BusStop("Pramuka LIA", new Position(106.868579f, -6.192215f), "4,6B"));
        busStops.add(new BusStop("Pulo Mas", new Position(106.892731f, -6.17471f), "10,2,2A,2B"));
        busStops.add(new BusStop("Pulogadung", new Position(106.909069f, -6.183362f), "2,2A,2B,4,6B"));
        busStops.add(new BusStop("Pulogebang", new Position(106.950949f, -6.211352f), "11"));
        busStops.add(new BusStop("Ragunan", new Position(106.820239f, -6.305223f), "6,6A,6B"));
        busStops.add(new BusStop("Rawa Buaya", new Position(106.726368f, -6.153962f), "2B,3,3A"));
        busStops.add(new BusStop("Rawa Selatan", new Position(106.85804f, -6.173918f), "2,2A,2B"));
        busStops.add(new BusStop("RS Harapan Bunda", new Position(106.868056f, -6.301902f), "7"));
        busStops.add(new BusStop("RS Harapan Kita", new Position(106.797033f, -6.184876f), "9,9A"));
        busStops.add(new BusStop("RS Islam", new Position(106.87043f, -6.168586f), "2,2A,2B"));
        busStops.add(new BusStop("RS Medika Permata Hijau", new Position(106.777521f, -6.218337f), "8"));
        busStops.add(new BusStop("RS Sumber Waras", new Position(106.796932f, -6.166264f), "2B,3,3A"));
        busStops.add(new BusStop("RS Tarakan", new Position(106.810079f, -6.171145f), "8A"));
        busStops.add(new BusStop("RSPAD", new Position(106.836727f, -6.175464f), "2,2A,2B"));
        busStops.add(new BusStop("S Parman Podomoro City", new Position(106.792484f, -6.175834f), "8A,9,9A"));
        busStops.add(new BusStop("Salemba Carolus", new Position(106.851109f, -6.196846f), "5,7A,7B"));
        busStops.add(new BusStop("Salemba UI", new Position(106.849056f, -6.193624f), "5,7A,7B"));
        busStops.add(new BusStop("Sarinah", new Position(106.822964f, -6.187966f), "1,2A,3A,6A"));
        busStops.add(new BusStop("Sawah Besar", new Position(106.819186f, -6.160241f), "1"));
        busStops.add(new BusStop("Semanggi", new Position(106.81338f, -6.220636f), "9,9A"));
        busStops.add(new BusStop("Senayan JCC", new Position(106.809054f, -6.214192f), "9,9A"));
        busStops.add(new BusStop("Senen", new Position(106.842642f, -6.178126f), "2,2A,2B"));
        busStops.add(new BusStop("Senen Sentral", new Position(106.842f, -6.17816f), "5"));
        busStops.add(new BusStop("Setiabudi", new Position(106.821209f, -6.210159f), "1,2A,3A"));
        busStops.add(new BusStop("Setiabudi Utara AINI", new Position(106.829863f, -6.20801f), "6,6A,6B"));
        busStops.add(new BusStop("Simprug", new Position(106.786597f, -6.233853f), "8"));
        busStops.add(new BusStop("Slamet Riyadi", new Position(106.859276f, -6.20856f), "5,7A,7B"));
        busStops.add(new BusStop("Slipi Kemanggisan", new Position(106.796985f, -6.189901f), "9,9A"));
        busStops.add(new BusStop("Slipi Petamburan", new Position(106.799958f, -6.201969f), "9,9A"));
        busStops.add(new BusStop("SMK 57", new Position(106.823521f, -6.291151f), "6,6A,6B"));
        busStops.add(new BusStop("Stasiun Grogol/Latumenten", new Position(106.790928f, -6.161128f), "9"));
        busStops.add(new BusStop("Stasiun Jatinegara", new Position(106.874155f, -6.215156f), "10"));
        busStops.add(new BusStop("Stasiun Jatinegara 2", new Position(106.868f, -6.21561f), "11"));
        busStops.add(new BusStop("Stasiun Klender", new Position(106.898243f, -6.213528f), "11"));
        busStops.add(new BusStop("Sumur Bor", new Position(106.719172f, -6.15304f), "2B,3,3A"));
        busStops.add(new BusStop("Sunan Giri", new Position(106.883681f, -6.193233f), "4,6B"));
        busStops.add(new BusStop("Sunter Boulevard Barat", new Position(106.889585f, -6.148756f), "12"));
        busStops.add(new BusStop("Sunter Karya", new Position(106.870846f, -6.137625f), "12"));
        busStops.add(new BusStop("Sunter Kelapa Gading", new Position(106.890779f, -6.142731f), "10,12"));
        busStops.add(new BusStop("Sunter SMP 140", new Position(106.859172f, -6.139763f), "12"));
        busStops.add(new BusStop("Taman Kota", new Position(106.7579f, -6.157167f), "2B,3,3A"));
        busStops.add(new BusStop("Taman Mini Garuda", new Position(106.881108f, -6.29014f), "9"));
        busStops.add(new BusStop("Tanah Kusir Kodim", new Position(106.781683f, -6.256996f), "8"));
        busStops.add(new BusStop("Tanah Merdeka", new Position(106.873933f, -6.308163f), "7"));
        busStops.add(new BusStop("Tanjung Priok", new Position(106.881737f, -6.109492f), "10,12"));
        busStops.add(new BusStop("Tebet BKPM", new Position(106.851637f, -6.243216f), "9,9A"));
        busStops.add(new BusStop("Tegal Parang", new Position(106.830572f, -6.238877f), "9,9A"));
        busStops.add(new BusStop("Tegalan", new Position(106.857058f, -6.202935f), "5,7A,7B"));
        busStops.add(new BusStop("Tomang Mandala", new Position(106.800091f, -6.177044f), "8A"));
        busStops.add(new BusStop("Tosari", new Position(106.823139f, -6.198365f), "1,2A,3A"));
        busStops.add(new BusStop("TU Gas", new Position(106.904989f, -6.192332f), "4,6B"));
        busStops.add(new BusStop("UNJ", new Position(106.880201f, -6.192863f), "4,6B"));
        busStops.add(new BusStop("Utan Kayu", new Position(106.864613f, -6.192722f), "4,6B"));
        busStops.add(new BusStop("Utan Kayu Rawamangun", new Position(106.874046f, -6.197414f), "10"));
        busStops.add(new BusStop("Velodrome", new Position(106.888128f, -6.193445f), "4,6B"));
        busStops.add(new BusStop("Walikota Jakarta Timur", new Position(106.945419f, -6.212521f), "11"));
        busStops.add(new BusStop("Walikota Jakarta Utara", new Position(106.893275f, -6.118812f), "10,12"));
        busStops.add(new BusStop("Warung Jati", new Position(106.829628f, -6.262286f), "6,6A,6B"));
        busStops.add(new BusStop("Yos Sudarso Kodamar", new Position(106.881767f, -6.161876f), "10"));

    }
}
